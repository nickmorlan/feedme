/**
  Handles our factory/objects
*/
angular.module('app')


/**
 The Projects factory handles saving and loading projects from local storage,
 also lets us save and load the last active project index
*/
.factory('storageService', function($firebase, $q) {
    var endpoint = 'https://feedmenow.firebaseio.com/'
    return {
        all: function(apiCall) {
            var deferred = $q.defer()
            var data = {}
            ref  = new Firebase(endpoint + apiCall)
            var obj = $firebase(ref).$asObject()
            ref.on("value", function(snapshot) {
                //console.log(snapshot.val())
                data = snapshot.val()
                deferred.resolve(data)
            }, function(errorObject) {
                console.log("The read failed: " + errorObject.code)
            })
            return deferred.promise
        },
        api: function(apiCall) {
            var deferred = $q.defer()
            var data = {}
            ref  = new Firebase(endpoint + apiCall)
            var obj = $firebase(ref).$asObject()
            ref.on("value", function(snapshot) {
                //console.log(snapshot.val())
                data = snapshot.val()
                deferred.resolve(data)
            }, function(errorObject) {
                console.log("The read failed: " + errorObject.code)
            })
            return deferred.promise
        }
    }
})

.factory('Place', function (storageService) {
    return {
        all: function() {
            return storageService.api("places")
        },
        getMenuFor: function(placeID) {
            return storageService.api("menus/" + placeID)
        },
        find: function(placeID) {
            return storageService.api("places/" + placeID)
        }
    }
})

.service('Order', function() {
    var items = []
    var total = 0

    var addItem = function(item) {
        this.items.push(item)
    }

    var removeItem = function(item) {
        if(this.items.indexOf(item) > -1) {
            this.items.splice(this.items.indexOf(item), 1)
        }
    }

    var total = function() {
        return 100
    }

    return {
        items: [],
        addItem: addItem,
        removeItem: removeItem,
        total: total

    }
})

.factory('Item', function(item) {
    return {
        prep: function() {

        }
    }
})

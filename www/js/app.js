// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('app', ['ionic',
                       'firebase',
                       'appControllers',
                       'appFilters'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

    .state('app', {
      abstract: true,
      url: '/',
      //templateUrl: 'index.html'
      template: "<ion-nav-view></ion-nav-view>",
    })

    .state('places', {
      url: '/places',
      templateUrl: 'templates/places.html',
      controller: 'PlaceCtrl'

    })

    .state('placeMenu', {
      url: '/place/:id/menu',
      templateUrl: 'templates/menu.html',
      controller: 'PlaceMenuCtrl'
    })

    .state('menuContent', {
      url: '/place/:id/menu/:category',
      templateUrl: 'templates/menu_content.html',
      controller: 'MenuCtrl'
    })

    .state('menuDetail', {
      url: '/place/:id/menu/:category/:itemID',
      templateUrl: 'templates/menu_detail.html',
      controller: 'MenuItemCtrl'
    })

    .state('cart', {
      url: '/place/:id/cart',
      templateUrl: 'templates/cart.html',
      controller: 'CartCtrl'
    })

    .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html'
    })
    ;

  // if none of the above states are matched, use this as the fallback

  $urlRouterProvider.otherwise('/login');


})

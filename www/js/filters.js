/* Custom Filters */
angular.module('appFilters', [])

.filter('price', function () {
    return function(input) {
        //console.log(input)]
        if(!input || input == 0) return
        return '$' + (input / 100).toFixed(2)
    }
})
angular.module('appControllers', [])

.controller('PlaceCtrl', ['$scope', '$stateParams', 'Place', 'storageService', '$firebase',
                          function($scope, $stateParams, Place, storageService, $firebase) {
    Place.all().then(function(data) { $scope.places = data })
    //console.log(Order)
}])

.controller('MenuCtrl', ['$scope', '$state', '$stateParams', 'Place', '$firebase', 'Order',
                         function($scope, $state, $stateParams, Place, $firebase, Order) {

    $scope.placeID  = $stateParams.id
    $scope.category = $stateParams.category
    $scope.order    = Order

    Place.getMenuFor($scope.placeID).then(function(data) {
        $scope.categories = data.category
    })
    Place.find($scope.placeID).then(function(data) {
        $scope.place = data
    })

}])

.controller('PlaceMenuCtrl', ['$scope', '$state', '$stateParams', 'Place', '$firebase', 'Order',
                         function($scope, $state, $stateParams, Place, $firebase, Order) {

    $scope.placeID = $stateParams.id
    $scope.order = Order
    Place.getMenuFor($scope.placeID).then(function(data) {
        $scope.categories = data.category;
    })
    Place.find($scope.placeID).then(function(data) {
        $scope.place = data
    })

}])

.controller('MenuItemCtrl', ['$scope', '$state', '$stateParams', 'Place', '$firebase', 'Order',
                         function($scope, $state, $stateParams, Place, $firebase, Order) {

    $scope.placeID = $stateParams.id
    $scope.order = Order
    Place.getMenuFor($scope.placeID).then(function(data) {
        $scope.item = data.category[$stateParams.category][$stateParams.itemID]
    })
    Place.find($scope.placeID).then(function(data) {
        $scope.place = data
    })

    $scope.updateItemFinalPrice = function() {
      if (this.option.price && this.option.selected) {
        $scope.item.finalPrice = parseInt($scope.item.finalPrice) + parseInt(this.option.price)
      } else {
        $scope.item.finalPrice = parseInt($scope.item.finalPrice) - parseInt(this.option.price)
      }
    }

    $scope.addToOrder = function(item) {
      Order.addItem(item)
      $state.go('placeMenu', {id: $scope.placeID})
    }

}])

.controller('CartCtrl', ['$scope', '$state', '$stateParams', 'Place', '$firebase', 'Order', '$ionicPopup',
                         function($scope, $state, $stateParams, Place, $firebase, Order, $ionicPopup) {

    $scope.placeID  = $stateParams.id
    $scope.order    = Order
    // make sure there are items in the 'cart'
    if($scope.order.items.length == 0) {
        $state.go('placeMenu', {id: $scope.placeID})
        return
    }

    Place.find($scope.placeID).then(function(data) {
        $scope.place = data
    })

    $scope.destroyOrderItem = function(index, item) {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Remove Item',
            template: 'Are you sure you want to remove ' + item.name + '?'
        })
        confirmPopup.then(function(res) {
            if(res) {
                $scope.order.items.splice(index, 1)
                if($scope.order.items.length == 0) {
                    $state.go('placeMenu', {id: $scope.placeID})
                }
            }
        })
    }

}])
